package com.agiletestingalliance;

import org.junit.Test;
import static org.junit.Assert.*;

public class MinMaxTest {

    @Test
    public void testminandmax1() throws Exception {

        int k= new MinMax().minandmax(10,20);
        assertEquals("test", 20, k);
    }
	
	@Test
    public void testminandmax2() throws Exception {

        int k= new MinMax().minandmax(20,10);
        assertEquals("test", 10, k);
    }
	
	@Test
    public void testbar1() throws Exception {

        String k= new MinMax().bar("");
        assertEquals("test", "", k);
    }
	
	@Test
    public void testbar2() throws Exception {

        String k= new MinMax().bar("a");
        assertEquals("test", "a", k);
    }
	
	@Test
    public void testbar3() throws Exception {

        String k= new MinMax().bar("krouse");
        assertEquals("test", "krouse", k);
    }
} 
 
